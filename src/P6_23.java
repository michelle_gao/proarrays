import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/13/13
 * Time: 2:47 PM
 * P6.23 Histogram: Write a program that reads a sequence of input values and displays a bar chart of the captions
 * and values.
 */
public class P6_23 {
    public static void main(String[] args) {
        System.out.println("Please enter the Captions: ");
        Scanner scCaptions = new Scanner(System.in);
        String[] strCaptions = scCaptions.nextLine().split(" ");
        System.out.println("Please enter the Values for Captions: ");
        Scanner scValues = new Scanner(System.in);
        String[] strValues = scValues.nextLine().split(" ");
        int nStarsPerNumber = starsPerNumber(strValues);
        String strStars = null;
        int nLength = strValues.length > strCaptions.length ? strCaptions.length : strValues.length;
        for (int i = 0; i < nLength; i++) {
            strStars = histGram(Integer.parseInt(strValues[i]),nStarsPerNumber);
            System.out.println(strCaptions[i] + " " + strStars);
        }
    }

    public static int starsPerNumber(String[] strValues) {
        int nValue;
        int maxValue;
        ArrayList<Integer> aValues = new ArrayList<Integer>();
        for (int i = 0; i < strValues.length; i++) {
            nValue = Integer.parseInt(strValues[i]);
            aValues.add(nValue);
        }
        if(Collections.max(aValues)<=40) {
            return 1;
        }
        else return (Collections.max(aValues)/40);

    }
    public static String histGram(int nValue, int nValuePerStar) {
        if (nValue < 1) {
            return "";
        }
        else {
            int nNumberOfStars = ((nValue+nValuePerStar-1) / nValuePerStar);
            String strStars = "*";
            for (int i = 1; i < nNumberOfStars; i++) {

                strStars = strStars + "*";
            }
            return strStars;
        }
    }
}
