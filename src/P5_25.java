import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/11/13
 * Time: 10:23 PM
 * P5_25 Postal bar codes
 */
public class P5_25 {
    public static void main(String[] args) {
        System.out.print("Please enter a zip code: ");
        Scanner sc = new Scanner(System.in);
        int nZipCode = sc.nextInt();
        printBarCode(nZipCode);
    }
    public static void printDigit(int d) {
        String strBarDigit = "";
        switch (d) {
            case 1:
                strBarDigit = "00011";break;
            case 2: strBarDigit="00101";break;
            case 3: strBarDigit = "00110";break;
            case 4: strBarDigit = "01001";break;
            case 5: strBarDigit = "01010";break;
            case 6: strBarDigit = "01100";break;
            case 7: strBarDigit = "10001";break;
            case 8: strBarDigit = "10010";break;
            case 9: strBarDigit = "10100";break;
            case 0: strBarDigit = "11000";break;
            default:break;
        }
        for ( int i=0;i<5;i++) {
            char cBarDigit = strBarDigit.charAt(i);
            switch (cBarDigit) {
                case '0':
                    System.out.print(':');break;
                case '1': System.out.print("|");break;
                default:break;
            }
        }
    }
    public static void printBarCode(int zipCode) {
        String strZipCode = Integer.toString(zipCode);
        int[] nDigits = new int[5];
        int nSum = 0;
        for (int i = 0; i < 5; i++) {
            nDigits[i] = Integer.parseInt(strZipCode.substring(i, i + 1));
            printDigit(nDigits[i]);
            nSum = (nSum + (nDigits[i]));
        }
        int nCheckCode;
        if (nSum % 10 == 0) {
            nCheckCode = 0;
        }
        else {
            int nTenth = nSum/10;
            nCheckCode = (nTenth+1)*10 - nSum;
        }
        printDigit(nCheckCode);
    }
}
