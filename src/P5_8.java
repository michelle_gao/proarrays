/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/11/13
 * Time: 6:17 PM
 * p5.8 Write a method String scramble(String word) that constructs a scrambles versions of given word,
 * randomly flipping two characters other than the first and last one. Then write a program that reads
 * words and prints the scrambles words.
 */
import java.util.Random;
import java.util.Scanner;
public class P5_8 {
    public static String scramble(String strWord) {
        Random rnd = new Random();
        int nScramble = rnd.nextInt((strWord.length() - 1));
        if(nScramble==0)nScramble++;
        if(nScramble == (strWord.length()-1))nScramble--;
        String strNewWord = strWord.substring(0, nScramble) + strWord.charAt(nScramble + 1)
                + strWord.charAt(nScramble) + strWord.substring(nScramble + 2);
        return strNewWord;
    }
    public static void main(String[] args) {
        System.out.print("Please enter words: ");
        Scanner sc = new Scanner(System.in);
        String strWordLine = sc.nextLine();
        String[] strWords = strWordLine.split(" ");
        for (String strWord : strWords) {
            String strFlip = strWord.length()>3? strFlip = scramble(strWord) : strWord;
            System.out.print(" " + strFlip + " ");
        }
        System.out.println();
        return;
    }
}
