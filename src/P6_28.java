import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/12/13
 * Time: 10:23 AM
 * P6.28 Write a method that merges two sorted array Lists, producing a new sorted array list.
 */
public class P6_28 {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        System.out.print("Please enter the first  sorted Array list or \"Q\" to quit: ");
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            a.add(sc.nextInt());
        }
        ArrayList<Integer> b = new ArrayList<Integer>();
        System.out.print("Please enter the Second sorted Array list or \"Q\" to quit: ");
        Scanner sc2 = new Scanner(System.in);
        while (sc2.hasNextInt()) {
            b.add(sc2.nextInt());
        }
        ArrayList<Integer> c = new ArrayList<Integer>();
        c = mergeSorted(a, b);
        System.out.println();
        int j = 0;
        String strNumber;
        for ( Integer nNumber : c) {
            System.out.print(nNumber + " ");
        }

    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        int nPosA = 0;
        int nPosB = 0;
        ArrayList<Integer> c = new ArrayList<Integer>();
        int nMiniSize = a.size()<b.size()? a.size():b.size();
        while ( nPosA < a.size() && nPosB < b.size()) {
            if ( a.get(nPosA)<b.get(nPosB)) {
                c.add(a.get(nPosA));
                nPosA++;
            }
            else {
                c.add(b.get(nPosB));
                nPosB++;
            }
        }
        for ( int i = nPosA; nPosA<a.size(); i++)   {
            c.add(a.get(nPosA));
            nPosA++;
        }
        for ( int i = nPosB; nPosB<b.size(); i++)   {
            c.add(b.get(nPosB));
            nPosB++;
        }
        return c;
    }
}
