import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/12/13
 * Time: 2:06 PM
 * P7.2 Write a  program that reads a file containing text. Read each line and send it to the output file, preceded by
 * line numbers.
 */
public class P7_2 {
    public static void main(String[] args) throws FileNotFoundException {
        JFileChooser chooser = new JFileChooser();
        Scanner sc = null;
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File fInputFile = chooser.getSelectedFile();
            sc = new Scanner(fInputFile);
            PrintWriter out = new PrintWriter("outputFile.txt");
            String strLine, strNewLine;
            int i = 1;
            while (sc.hasNextLine()) {
                strLine = sc.nextLine();
                strNewLine = "/* " + i + " */ " + strLine;
                out.println(strNewLine);
                i++;
            }
            sc.close();
            out.close();
        }
    }


}
