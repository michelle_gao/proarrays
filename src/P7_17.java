import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/13/13
 * Time: 1:02 PM
 * P7.17 Write a program that reads a text file as described in p7.16 and that writes a separate file for each service
 * category, containing the entries for that category. Name the output files Dinner.txt, Conference.txt and Lodging.ext.
 */
public class P7_17 {
    public enum Services {

        DINNER,
        CONFERENCE,
        LODGING
    }
    public static void main(String[] args) throws FileNotFoundException {
        File fHotelSale = new File("hotelSale.txt");
        Scanner sc = new Scanner(fHotelSale);
        PrintWriter pDinner = new PrintWriter("Dinner.txt");
        PrintWriter pConference = new PrintWriter("Conference.txt");
        PrintWriter pLodging  = new PrintWriter("Lodging.txt");
        String strLine = null;
        String[] strEntries = null;
        Services CurrentSer = null;
        String current = null;
        String strNewLine = null;
        while (sc.hasNextLine()) {
            strLine = sc.nextLine();
            strEntries = strLine.split(";");
            current = strEntries[1];
            CurrentSer = Services.valueOf(current.toUpperCase());
            switch (CurrentSer) {
                case DINNER:
                    strNewLine = strEntries[0] + ";" + strEntries[2] + ";" + strEntries[3];
                    pDinner.println(strNewLine);
                    break;
                case CONFERENCE:
                    strNewLine = strEntries[0] + ";" + strEntries[2] + ";" + strEntries[3];
                    pConference.println(strNewLine);
                    break;
                case LODGING:
                    strNewLine = strEntries[0] + ";" + strEntries[2] + ";" + strEntries[3];
                    pLodging.println(strNewLine);
                    break;
            }
        }
        sc.close();
        pDinner.close();
        pLodging.close();
        pConference.close();
    }

}
