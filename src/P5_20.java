import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/11/13
 * Time: 10:12 PM
 * Write a method to test whether a year is a leap year.
 */
public class P5_20 {
    public static void main(String[] args) {
        System.out.print("Please enter a year: ");
        Scanner sc = new Scanner(System.in);
        int nYear = sc.nextInt();
        boolean isLeap = isLeapYear(nYear);
        String strIsLeap = isLeap ? "is" : "isn't";
        System.out.println("The year " + nYear + " " + strIsLeap + " a leap year.");
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 != 0) return true;
            else {
                if (year % 400 == 0) return true;
                else return  false;
            }
        }
        else return false;
    }
}
