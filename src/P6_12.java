import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/12/13
 * Time: 9:02 AM
 * P6.12 Translating pseudocode with arrays
 */
public class P6_12 {
    public static void main(String[] args) {
        int[] nDieTosses = generateDies();
        boolean inRun = false;
        for (int i = 1; i < 19; i++) {
            if (inRun) {
                if(nDieTosses[i]!= nDieTosses[i-1])   {
                    System.out.print(")");
                    inRun = false;
                }
            }
            if (!inRun) {
                if(nDieTosses[i]==nDieTosses[i+1]) {
                    System.out.print("(");
                    inRun = true;
                }
            }
            System.out.print(nDieTosses[i] + " ");
        }
        System.out.print(nDieTosses[19]);
        if( inRun) System.out.print(")");

    }

    public static int[] generateDies() {
        Random rnc = new Random();
        int[] nDies = new int[20];
        for (int i = 0; i < 20; i++) {
             nDies[i] = rnc.nextInt(10);
        }
        return nDies;
    }

}
